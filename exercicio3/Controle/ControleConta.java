package exercicio3.Controle;

//import java.util.ArrayList;

import exercicio3.modelos.*;

public class ControleConta {
	//private ArrayList<Cliente> listaClientes;
	public ControleConta() {
		//listaClientes = new ArrayList();
	}
	
	public double conferirSaldo(Conta conta) {
		return conta.getSaldo();
	}
	public String depositar(Conta conta, double quantia) {
		double novoSaldo = conta.getSaldo() + quantia;
		conta.setSaldo( novoSaldo );
		return "Novo Saldo: RS"+ novoSaldo;
	}
	public String sacar(Conta conta, double quantia) {
		double novoSaldo = conta.getSaldo() - quantia;
		conta.setSaldo( novoSaldo );
		return "Novo Saldo: RS"+ novoSaldo;
	}
	public String transferir(Conta conta1, Conta conta2, double quantia) {
		double novoSaldo1 = conta1.getSaldo() - quantia;
		double novoSaldo2 = conta2.getSaldo() + quantia;
		conta1.setSaldo( novoSaldo1 );
		conta2.setSaldo( novoSaldo2 );
		return "Novo Saldo: RS"+ novoSaldo1;
	}
	public String aplicarEmFundo(Conta conta, Aplica��oEmFundo fundo, double quantia) {
		double novoSaldo = conta.getSaldo() - quantia;
		fundo.setQuantiaAplicada(quantia);
		return "Novo Saldo: RS"+ novoSaldo;
	}
}
