package exercicio3.modelos;

public class ContaPoupança extends Conta {
	public ContaPoupança(Cliente titular) {
		super(titular);
	}
	
	private double TaxaDeJurosMensal = 0.6;
	private boolean AprovaçãoParaSaque = false;
	
	public double getTaxaDeJurosMensal() {
		return TaxaDeJurosMensal;
	}
	public void setTaxaDeJurosMensal(double taxaDeJurosMensal) {
		TaxaDeJurosMensal = taxaDeJurosMensal;
	}
	public boolean isAprovaçãoParaSaque() {
		return AprovaçãoParaSaque;
	}
	public void setAprovaçãoParaSaque(boolean aprovaçãoParaSaque) {
		AprovaçãoParaSaque = aprovaçãoParaSaque;
	}
}
