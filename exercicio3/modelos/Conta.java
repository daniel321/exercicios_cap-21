package exercicio3.modelos;

public class Conta {
	public Conta(Cliente titular) {
		this.Titular = titular;
	}
	public Conta(Cliente titular, int numero) {
		this.Titular = titular;
		this.N�mero = numero;
	}
	
	private Cliente Titular;
	private int N�mero;
	private int Agencia;
	private String DataDeAbertura;
	private double Saldo;
	
	public Cliente getTitular() {
		return Titular;
	}
	public void setTitular(Cliente titular) {
		Titular = titular;
	}
	public int getN�mero() {
		return N�mero;
	}
	public void setN�mero(int n�mero) {
		N�mero = n�mero;
	}
	public int getAgencia() {
		return Agencia;
	}
	public void setAgencia(int agencia) {
		Agencia = agencia;
	}
	public String getDataDeAbertura() {
		return DataDeAbertura;
	}
	public void setDataDeAbertura(String dataDeAbertura) {
		DataDeAbertura = dataDeAbertura;
	}
	public double getSaldo() {
		return Saldo;
	}
	public void setSaldo(double saldo) {
		Saldo = saldo;
	}
}
