package exercicio3.modelos;

public class Cliente 
{
    private String nome;
    private String telefone;
    private String endereco;
    
     public Cliente() {
     }
    
     public Cliente(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone ;
     }
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getString() {
        return endereco;
    }

    public void setString(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
