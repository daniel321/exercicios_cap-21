package exercicio3.modelos;

public class PessoaJuridica extends Cliente{
	public PessoaJuridica(String nome, String telefone,String cnpj,String razaoSocial) {
		super(nome, telefone);
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
	}
	
    private  String cnpj;
    private  String razaoSocial;
    private  String tamanhoEmpresa = "Normal";

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

	public String getTamanhoEmpresa() {
		return tamanhoEmpresa;
	}

	public void setTamanhoEmpresa(String tamanhoEmpresa) {
		this.tamanhoEmpresa = tamanhoEmpresa;
	}
}
