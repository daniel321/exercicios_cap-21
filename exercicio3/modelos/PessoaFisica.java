package exercicio3.modelos;

public class PessoaFisica extends Cliente{
	public PessoaFisica(String nome, String telefone,String cpf, String emprego) {
		super(nome, telefone);
        this.cpf = cpf;
        this.ocupa��o = emprego;
        
	}
	
    private  String cpf;
    private  boolean maiorIdade = true;
    private  String ocupa��o;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

	public boolean isMaiorIdade() {
		return maiorIdade;
	}

	public void setMaiorIdade(boolean maiorIdade) {
		this.maiorIdade = maiorIdade;
	}

	public String getOcupa��o() {
		return ocupa��o;
	}

	public void setOcupa��o(String emprego) {
		this.ocupa��o = emprego;
	}
}
