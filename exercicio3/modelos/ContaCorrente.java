package exercicio3.modelos;

public class ContaCorrente extends Conta {
	public ContaCorrente(Cliente titular) {
		super(titular);
	}
	private double Saldo;
	private double TaxaDeManutenção = 1.0;
	
	public double getSaldo() {
		return Saldo;
	}
	public void setSaldo(double saldo) {
		Saldo = saldo;
	}
	public double getTaxaDeManutenção() {
		return TaxaDeManutenção;
	}
	public void setTaxaDeManutenção(double taxaDeManutenção) {
		TaxaDeManutenção = taxaDeManutenção;
	}
	
}
